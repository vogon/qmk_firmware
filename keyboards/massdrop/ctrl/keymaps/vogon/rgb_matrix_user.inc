RGB_MATRIX_EFFECT(rainy_splash)

#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS

#ifdef CONSOLE_ENABLE
#define DEBUG_PRINT(fmt, ...) \
if (debug_enable) { \
    uprintf(fmt, ## __VA_ARGS__); \
}
#else /* !CONSOLE_ENABLE */
#define DEBUG_PRINT(fmt, ...)
#endif

// lifted from quantum/rgb_matrix_animations/solid_splash_anim.h
HSV splash_math(HSV hsv, int16_t dx, int16_t dy, uint8_t dist, uint16_t tick) {
    uint16_t effect = tick - dist;
    if (effect > 255) effect = 255;
    hsv.v = qadd8(hsv.v, 255 - effect);
    return hsv;
}

#ifdef VFX_TWEAK
extern uint8_t g_vfx_params[];
#endif

// use the framebuffer to store the "background" raindrops, then the hit
// trackers to store the "foreground" ones, and blend
static bool rainy_splash(effect_params_t *params) {
#ifdef VFX_TWEAK
    const uint8_t bg_update_period = g_vfx_params[0];
    const uint8_t bg_drops_per_frame = g_vfx_params[1];
    const uint8_t bg_drop_starting_intensity = g_vfx_params[2];
    const uint8_t bg_drop_decay = g_vfx_params[3];
#else /* !VFX_TWEAK */
    const uint8_t bg_update_period = 2;
    const uint8_t bg_drops_per_frame = 3;
    const uint8_t bg_drop_starting_intensity = 28;
    const uint8_t bg_drop_decay = 1;
#endif /* #ifdef VFX_TWEAK */
    static uint8_t frame_counter = 0;

    if (params->init && (params->iter == 0)) {
        DEBUG_PRINT("rainy_splash: MATRIX_COLS is %u, MATRIX_ROWS is %u, "
            "rand() is %u %u %u\n", MATRIX_COLS, MATRIX_ROWS, rand(), rand(), rand());
        rgb_matrix_set_color_all(0, 0, 0);
        memset(g_rgb_frame_buffer, 0, sizeof(g_rgb_frame_buffer));
    }

    // only update the background fb once per frame
    if (params->iter == 0) {
        if (++frame_counter % bg_update_period == 0) {
            // make new background raindrops
            for (uint8_t i = 0; i < bg_drops_per_frame; i++) {
                uint8_t col = rand() % MATRIX_COLS;
                uint8_t row = rand() % MATRIX_ROWS;
                // DEBUG_PRINT("rainy_splash: new drop at %u, %u\n", col, row);

                g_rgb_frame_buffer[row][col] = qadd8(g_rgb_frame_buffer[row][col],
                    bg_drop_starting_intensity);
            }
        }
    }

    // Modified version of RGB_MATRIX_USE_LIMITS to work off of matrix row / col size;
    // lifted from quantum/rgb_matrix_animations/typing_heatmap_anim.h
    uint8_t led_min = RGB_MATRIX_LED_PROCESS_LIMIT * params->iter;
    uint8_t led_max = led_min + RGB_MATRIX_LED_PROCESS_LIMIT;
    if (led_max > sizeof(g_rgb_frame_buffer)) led_max = sizeof(g_rgb_frame_buffer);

    // render the raindrops and decay; also render the splashes
    // DEBUG_PRINT("rainy_splash: decay %u..%u\n", led_min, led_max);
    uint8_t count = g_last_hit_tracker.count;
    for (int i = led_min; i < led_max; i++) {
        RGB_MATRIX_TEST_LED_FLAGS();
        HSV hsv = rgb_matrix_config.hsv;
        hsv.v = 0;

        uint8_t row = i % MATRIX_ROWS;
        uint8_t col = i / MATRIX_ROWS;
        uint8_t bg_intensity = g_rgb_frame_buffer[row][col];

        uint8_t led[LED_HITS_TO_REMEMBER];
        uint8_t led_count = rgb_matrix_map_row_column_to_led(row, col, led);

        for (uint8_t matching_led = 0; matching_led < led_count; matching_led++) {
            // blend in the splash color
            for (uint8_t hit = 0; hit < count; hit++) {
                int16_t dx = g_led_config.point[led[matching_led]].x - g_last_hit_tracker.x[hit];
                int16_t dy = g_led_config.point[led[matching_led]].y - g_last_hit_tracker.y[hit];
                uint8_t dist = sqrt16(dx * dx + dy * dy);
                uint16_t tick = scale16by8(g_last_hit_tracker.tick[hit], rgb_matrix_config.speed);
                hsv = splash_math(hsv, dx, dy, dist, tick);
            }

            hsv.v = scale8(hsv.v, rgb_matrix_config.hsv.v);

            // approximate blending by using the primary color
            // saturation if the foreground splash drowns out the background
            // brightness, and zero saturation if not
            HSV final_hsv = { h: hsv.h, s: scale8(hsv.s, hsv.v),
                v: qadd8(hsv.v, bg_intensity) };
            RGB final_rgb = rgb_matrix_hsv_to_rgb(final_hsv);

            rgb_matrix_set_color(led[matching_led], final_rgb.r,
                final_rgb.g, final_rgb.b);
        }

        if (frame_counter % bg_update_period == 0) {
            g_rgb_frame_buffer[row][col] = qsub8(bg_intensity, bg_drop_decay);
        }
    }

    return led_max < sizeof(g_rgb_frame_buffer);
}

#endif
