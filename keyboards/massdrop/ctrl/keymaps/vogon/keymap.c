#include QMK_KEYBOARD_H

#include <lib/lib8tion/lib8tion.h>

enum layers {
    _DEF = 0,
    _CTL,
    _VFX
};

enum ctrl_keycodes {
    U_T_AUTO = SAFE_RANGE, //USB Extra Port Toggle Auto Detect / Always Active
    U_T_AGCR,              //USB Toggle Automatic GCR control
    DBG_TOG,               //DEBUG Toggle On / Off
    DBG_MTRX,              //DEBUG Toggle Matrix Prints
    DBG_KBD,               //DEBUG Toggle Keyboard Prints
    DBG_MOU,               //DEBUG Toggle Mouse Prints
    MD_BOOT,               //Restart into bootloader after hold timeout
    C_SET_R,
    C_SET_O,
    C_SET_Y,
    C_SET_G,
    C_SET_B,
    C_SET_V,
    C_SET_W,
    C_SET_K,
#ifdef VFX_TWEAK
    VFX_1I,
    VFX_2I,
    VFX_3I,
    VFX_4I,
    VFX_1D,
    VFX_2D,
    VFX_3D,
    VFX_4D,
    VFX_DMP,
#endif /* VFX_TWEAK */
};

keymap_config_t keymap_config;

#ifdef VFX_TWEAK
#define VFX_LAY TG(_VFX)
#else
#define VFX_LAY _______
#endif

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_DEF] = LAYOUT(
        KC_ESC,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,             KC_PSCR, KC_SLCK, KC_PAUS, \
        KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC,   KC_INS,  KC_HOME, KC_PGUP, \
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS,   KC_DEL,  KC_END,  KC_PGDN, \
        KC_CAPS, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, KC_ENT, \
        KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,                              KC_UP, \
        KC_LCTL, KC_LGUI, KC_LALT,                   KC_SPC,                             KC_RALT, MO(_CTL),KC_APP,  KC_RCTL,            KC_LEFT, KC_DOWN, KC_RGHT \
    ),
    [_CTL] = LAYOUT(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,            KC_MUTE, _______, _______, \
        _______, C_SET_R, C_SET_O, C_SET_Y, C_SET_G, C_SET_B, C_SET_V, C_SET_W, C_SET_K, _______, _______, _______, _______, _______,   KC_MPLY, KC_MSTP, KC_VOLU, \
        _______, RGB_SPD, RGB_VAI, RGB_SPI, RGB_HUI, RGB_SAI, _______, U_T_AUTO,U_T_AGCR,_______, _______, _______, _______, _______,   KC_MPRV, KC_MNXT, KC_VOLD, \
        _______, RGB_RMOD,RGB_VAD, RGB_MOD, RGB_HUD, RGB_SAD, _______, _______, DBG_TOG, _______, _______, _______, _______, \
        _______, RGB_TOG, _______, _______, _______, MD_BOOT, NK_TOGG, _______, VFX_LAY, _______, _______, _______,                              _______, \
        _______, _______, _______,                   _______,                            _______, _______, _______, _______,            _______, _______, _______ \
    ),
#ifdef VFX_TWEAK
    [_VFX] = LAYOUT(
        TG(_VFX),_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,            _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   _______, _______, _______, \
        _______, VFX_1I,  VFX_2I,  VFX_3I,  VFX_4I,  _______, _______, _______, _______, _______, VFX_DMP, _______, _______, _______,   _______, _______, _______, \
        _______, VFX_1D,  VFX_2D,  VFX_3D,  VFX_4D,  _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, NK_TOGG, _______, _______, _______, _______, _______,                              _______, \
        _______, _______, _______,                   _______,                            _______, _______, _______, _______,            _______, _______, _______ \
    ),
#endif /* VFX_TWEAK */
    /*
    [X] = LAYOUT(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,            _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, NK_TOGG, _______, _______, _______, _______, _______,                              _______, \
        _______, _______, _______,                   _______,                            _______, _______, _______, _______,            _______, _______, _______ \
    ),
    */
};

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
};

// Runs constantly in the background, in a loop.
void matrix_scan_user(void) {
};

#define MODS_SHIFT  (get_mods() & MOD_MASK_SHIFT)
#define MODS_CTRL   (get_mods() & MOD_MASK_CTRL)
#define MODS_ALT    (get_mods() & MOD_MASK_ALT)

#ifdef CONSOLE_ENABLE
#define DEBUG_PRINT(fmt, ...) \
if (debug_enable) { \
    uprintf(fmt, ## __VA_ARGS__); \
}
#else /* !CONSOLE_ENABLE */
#define DEBUG_PRINT(fmt, ...)
#endif

#if !defined(RGB_MATRIX_HUE_STEP)
#    define RGB_MATRIX_HUE_STEP 8
#endif

#if !defined(RGB_MATRIX_SAT_STEP)
#    define RGB_MATRIX_SAT_STEP 16
#endif

#if !defined(RGB_MATRIX_VAL_STEP)
#    define RGB_MATRIX_VAL_STEP 16
#endif

layer_state_t layer_state_set_user(layer_state_t state) {
    DEBUG_PRINT("layer_state_set_user: switching to layer %u\n", state);
    return state;
}

void rgb_matrix_indicators_user(void) {
    switch (get_highest_layer(layer_state)) {
#ifdef VFX_TWEAK
    case _VFX:
        rgb_matrix_set_color(0, RGB_RED); // esc
        break;
#endif /* VFX_TWEAK */
    case _CTL:
        // mnemonics for function-layer keys
        rgb_matrix_set_color_all(RGB_BLACK);

        HSV current_color = rgb_matrix_get_hsv();
        rgb_matrix_set_color(17, RGB_RED); // 1
        rgb_matrix_set_color(18, RGB_ORANGE); // 2
        rgb_matrix_set_color(19, RGB_YELLOW); // 3
        rgb_matrix_set_color(20, RGB_GREEN); // 4
        rgb_matrix_set_color(21, RGB_BLUE); // 5
        rgb_matrix_set_color(22, RGB_PURPLE); // 6
        rgb_matrix_set_color(23, RGB_WHITE); // 7
        rgb_matrix_set_color(24, RGB_BLACK); // 8

        HSV hue_inc_hsv = { h: current_color.h + RGB_MATRIX_HUE_STEP,
            s: current_color.s, v: current_color.v };
        HSV hue_dec_hsv = { h: current_color.h - RGB_MATRIX_HUE_STEP,
            s: current_color.s, v: current_color.v };
        RGB hue_inc_rgb = hsv_to_rgb(hue_inc_hsv);
        RGB hue_dec_rgb = hsv_to_rgb(hue_dec_hsv);

        rgb_matrix_set_color(37, hue_inc_rgb.r, hue_inc_rgb.g, hue_inc_rgb.b); // R
        rgb_matrix_set_color(54, hue_dec_rgb.r, hue_dec_rgb.g, hue_dec_rgb.b); // F

        HSV sat_inc_hsv = { h: current_color.h,
            s: qadd8(current_color.s, RGB_MATRIX_SAT_STEP), v: current_color.v };
        HSV sat_dec_hsv = { h: current_color.h,
            s: qsub8(current_color.s, RGB_MATRIX_SAT_STEP), v: current_color.v };
        RGB sat_inc_rgb = hsv_to_rgb(sat_inc_hsv);
        RGB sat_dec_rgb = hsv_to_rgb(sat_dec_hsv);

        rgb_matrix_set_color(38, sat_inc_rgb.r, sat_inc_rgb.g, sat_inc_rgb.b); // T
        rgb_matrix_set_color(55, sat_dec_rgb.r, sat_dec_rgb.g, sat_dec_rgb.b); // G

        HSV value_inc_hsv = { h: current_color.h, s: current_color.s,
            v: qadd8(current_color.v, RGB_MATRIX_VAL_STEP) };
        HSV value_dec_hsv = { h: current_color.h, s: current_color.s,
            v: qsub8(current_color.v, RGB_MATRIX_VAL_STEP) };
        RGB value_inc_rgb = hsv_to_rgb(value_inc_hsv);
        RGB value_dec_rgb = hsv_to_rgb(value_dec_hsv);

        rgb_matrix_set_color(35, value_inc_rgb.r, value_inc_rgb.g, value_inc_rgb.b); // W
        rgb_matrix_set_color(52, value_dec_rgb.r, value_dec_rgb.g, value_dec_rgb.b); // S
        break;
    }
}

#ifdef VFX_TWEAK
uint8_t g_vfx_params[] = { 1, 8, 20, 1 };
#endif /* VFX_TWEAK */

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    static uint32_t key_timer;

    DEBUG_PRINT("KL: kc: %u, col: %u, row: %u, pressed: %u\n", keycode,
            record->event.key.col, record->event.key.row, record->event.pressed);

    switch (keycode) {
        case U_T_AUTO:
            if (record->event.pressed && MODS_SHIFT && MODS_CTRL) {
                TOGGLE_FLAG_AND_PRINT(usb_extra_manual, "USB extra port manual mode");
            }
            return false;
        case U_T_AGCR:
            if (record->event.pressed && MODS_SHIFT && MODS_CTRL) {
                TOGGLE_FLAG_AND_PRINT(usb_gcr_auto, "USB GCR auto mode");
            }
            return false;
        case DBG_TOG:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_enable, "Debug mode");
            }
            return false;
        case DBG_MTRX:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_matrix, "Debug matrix");
            }
            return false;
        case DBG_KBD:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_keyboard, "Debug keyboard");
            }
            return false;
        case DBG_MOU:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_mouse, "Debug mouse");
            }
            return false;
        case MD_BOOT:
            if (record->event.pressed) {
                key_timer = timer_read32();
            } else {
                if (timer_elapsed32(key_timer) >= 500) {
                    reset_keyboard();
                }
            }
            return false;
        case RGB_TOG:
            if (record->event.pressed) {
              switch (rgb_matrix_get_flags()) {
                case LED_FLAG_ALL: {
                    rgb_matrix_set_flags(LED_FLAG_KEYLIGHT | LED_FLAG_MODIFIER | LED_FLAG_INDICATOR);
                    rgb_matrix_set_color_all(0, 0, 0);
                  }
                  break;
                case (LED_FLAG_KEYLIGHT | LED_FLAG_MODIFIER | LED_FLAG_INDICATOR): {
                    rgb_matrix_set_flags(LED_FLAG_UNDERGLOW);
                    rgb_matrix_set_color_all(0, 0, 0);
                  }
                  break;
                case LED_FLAG_UNDERGLOW: {
                    rgb_matrix_set_flags(LED_FLAG_NONE);
                    rgb_matrix_disable_noeeprom();
                  }
                  break;
                default: {
                    rgb_matrix_set_flags(LED_FLAG_ALL);
                    rgb_matrix_enable_noeeprom();
                  }
                  break;
              }
            }
            return false;
        case C_SET_R...C_SET_K:
        {
            HSV colors[] = { { HSV_RED }, { HSV_ORANGE }, { HSV_YELLOW },
                { HSV_GREEN }, { HSV_BLUE }, { HSV_PURPLE }, { HSV_WHITE },
                { HSV_BLACK } };
            HSV color = colors[keycode - C_SET_R];

            if (record->event.pressed) {
                rgb_matrix_sethsv(color.h, color.s, color.v);
            }

            return false;
        }
#ifdef VFX_TWEAK
        case VFX_1I...VFX_4I:
            DEBUG_PRINT("VFX_%uI\n", keycode - VFX_1I + 1);
            if (record->event.pressed) {
                g_vfx_params[keycode - VFX_1I]++;
            }
            return false;
        case VFX_1D...VFX_4D:
            DEBUG_PRINT("VFX_%uD\n", keycode - VFX_1D + 1);
            if (record->event.pressed) {
                g_vfx_params[keycode - VFX_1D]--;
            }
            return false;
        case VFX_DMP:
            DEBUG_PRINT("vfx params: %u, %u, %u, %u\n", g_vfx_params[0],
                g_vfx_params[1], g_vfx_params[2], g_vfx_params[3]);
            return false;
#endif /* VFX_TWEAK */
        default:
            return true; //Process all other keycodes normally
    }
}
